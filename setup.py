#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright @ 2014 Mitchell Chu

import io

import torndghsession

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


version = torndghsession.version

setup(
    name='torndghsession',
    version=version,
    description="Session extensions for Tornado",
    long_description="给予https://github.com/MitchellChu/torndsession进行一些开发",
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Topic :: Internet :: WWW/HTTP :: Session',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
    ],
    license="MIT",
    packages=["torndghsession"],
    include_package_data=True,
    zip_safe=True,
    install_requires=['tornado',],
)
