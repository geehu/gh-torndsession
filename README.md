### 说明
修改自[https://github.com/MitchellChu/torndsession](https://github.com/MitchellChu/torndsession)  
主要支持设置自动更新存储介质(force_persistence=True)时的使用，非自动更新存储介质时，需要在必要的地方手动调用BaseSessionHandler.session.refresh()方法，以防止出现根据匹配项批量更新session后，可能会被旧数据覆盖的问题，见更新记录[v1.1.7#5](#v1.1.7)。
### 主要更新
####v1.1.6  
1. 增加按照条件删除的功能***`SessionBaseHandler.clear_session_by_match(**kwargs)`***，以便在某种情况下删除特定的session，比如用户修改密码后，清空所有该用户的session
2.  增加删除session的功能***`SessionBaseHandler.clear_session_obj(self, session_id=None)`***，不传入session_id则在session的存储位置中删除self.session
####v1.1.7  
1.  删除MemcacheDriver，我们的应用场景不使用该数据库
2.  增加刷新session过期时间的方法***`SessionBaseHandler.session.refresh_expires()`***，调用该方法会将session的过期时间按照以当前时间为session的生成时间更新session的过期时间。  
主要应用场景为XX时间内没有进行YY操作，则过期session，将session过期时间设为XX，进行YY操作的handler中调用refresh_expires方法即可实现。
3.  增加根据匹配项批量刷新session过期时间的方法***`SessionBaseHandler.refresh_session_expires_by_match(**kwargs)`***
4.  增加根据匹配项批量更新session的方法***`SessionBaseHandler.refresh_session_expires_by_match(update_data, **kwargs)`***  
5.  为了避免出现直接在存储数据中修改了session之前，BaseHandler.session对象就已经生成了，但是在handler中对session进行了修改时，调用sessio，为修改的内容会用旧数据进行了覆盖，所以增加了***`BaseSessionHandler.session.refresh()`***方法，该方法忽略当前session所有未提交的修改，重新从存储对象中生成session对象，并且在设置强制更新(force_persistence=True)时，会在每次写到数据存储介质前先调用此方法
6.  增加WebSocketHandler的session handler:***`SessionWebSocketHandler`***，之前SessionBaseHandler中的方法在此对象中通用，其实现的代码从***`torndghsession.sessionhandler.SessionBaseHandler`***移动到***`torndghsession.session.SessionMixin`***类中，该类是***`SessionBaseHandler`***和***`SessionWebSocketHandler`***共同的父类  
7.  session设置中force_persistence字段默认为True  
8.  ***`SessionBaseHandler`***和***`SessionWebSocketHandler`***增加根据条件查询满足条件的所有session的方法***`find_sessions(**kwargs)`***
9.  增加手动设置session过期时间的方法***`torndghsession.session.SessionManager.set_expiration(seconds)`***  
### Examples  

```python
from tornado.gen import coroutine
from tornado.web import authenticated
from torndghsession.sessionhandler import SessionBaseHandler

@coroutine
class LoginHandler(SessionBaseHandler):
    def post(self):
        # ...
        self.session["username"] = "user1"
        self.session.set_expiration(86400)  # 24 hours
        # ...

@coroutine
@authenticated
class ModifyPwdHandler(SessionBaseHandler):
    def post(self):
        # ...
        self.clear_session_by_match(username=self.session.get('username'))
        # ...
        
@coroutine
@authenticated
class KeepAliveHandler(SessionBaseHandler):
    def post(self):
        # ...
        # if one user, mutiple session
        self.refresh_session_expires_by_match(username=self.session.get('username'))
        # if one user, one session
        self.session.refresh_expires()
        # ...
        
@coroutine
@authenticated
class OnlineStatusHandler(SessionBaseHandler):
    def post(self):
        # ...
        status = self.request.post.get("status")
        assert status in ("online", "offline", "leave")
        self.update_session_by_match(
            {"online_status": status},
            username=self.session.get('username')
        )
        # ...
```