#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright @ 2014 Mitchell Chu

from __future__ import (absolute_import, division, print_function,
                        with_statement)

from copy import copy
from datetime import datetime, timedelta

from torndghsession.driver import SessionDriver, ExpiresNotChange

import redis

try:
    import cPickle as pickle    # py2
except ImportError:
    import pickle               # py3


class RedisSession(SessionDriver):
    """
    Use Redis to save session object.
    """

    def get(self, session_id):
        self.__create_redis_client()
        session_data = self.client.get(session_id)
        if not session_data:
            return {}
        return pickle.loads(session_data)

    def save(self, session_id, session_data, expires=None):
        session_data = session_data if session_data else {}
        if expires and not isinstance(expires, ExpiresNotChange):
            session_data.update(__expires__=expires)
        if isinstance(expires, ExpiresNotChange):
            expires = session_data.get("__expires__")
        session_data = pickle.dumps(session_data)
        self.__create_redis_client()
        self.client.set(session_id, session_data)
        if expires:
            delta_seconds = int((expires - datetime.utcnow()).total_seconds())
            self.client.expire(session_id, delta_seconds)

    def clear(self, session_id):
        self.__create_redis_client()
        self.client.delete(session_id)

    def find_sessions(self, **kwargs):
        self.__create_redis_client()
        matched_sessions = {}
        all_session_id = (x for x in self.client.keys())
        for session_id in all_session_id:
            try:
                session_data = self.get(session_id)
                if all(session_data.get(k, None) == v for k, v in kwargs.items()):
                    matched_sessions[session_id] = session_data
            except pickle.UnpicklingError:
                pass
        return matched_sessions

    def clear_by_match(self, **kwargs):
        self.__create_redis_client()
        all_session_id = (x for x in self.client.keys())
        for session_id in all_session_id:
            try:
                session_data = self.get(session_id)
                if all(session_data.get(k, None) == v for k, v in kwargs.items()):
                    self.clear(session_id)
            except pickle.UnpicklingError:
                #  理论上session应该在一个独立的数据库中，但是为了防止该数据库有其他数据出现错误，做一下长度的检查,以及忽略无法解pickle的数据
                pass

    def update_by_match(self, update_data, **kwargs):
        self.__create_redis_client()
        all_session_id = (x for x in self.client.keys())
        for session_id in all_session_id:
            try:
                session_data = self.get(session_id)
                if all(session_data.get(k, None) == v for k, v in kwargs.items()):
                    session_data.update(update_data)
                    self.save(session_id, session_data, ExpiresNotChange())
            except pickle.UnpicklingError:
                #  理论上session应该在一个独立的数据库中，但是为了防止该数据库有其他数据出现错误，做一下长度的检查,以及忽略无法解pickle的数据
                pass

    def refresh_expires_by_match(self, **kwargs):
        self.__create_redis_client()
        new_expires = datetime.utcnow() + timedelta(seconds=self.session_settings['session_lifetime'])
        for session_id in self.client.keys():
            try:
                session_data = self.get(session_id)
                if all(session_data.get(k, None) == v for k, v in kwargs.items()):
                    self.save(session_id, session_data, new_expires)
            except pickle.UnpicklingError:
                #  理论上session应该在一个独立的数据库中，但是为了防止该数据库有其他数据出现错误，做一下长度的检查,以及忽略无法解pickle的数据
                pass

    def remove_expires(self):
        pass

    def __create_redis_client(self):
        if not hasattr(self, 'client'):
            if 'max_connections' in self.settings:
                connection_pool = redis.ConnectionPool(**self.settings)
                settings = copy(self.settings)
                del settings['max_connections']
                settings['connection_pool'] = connection_pool
            else:
                settings = self.settings
            self.client = redis.Redis(**settings)
